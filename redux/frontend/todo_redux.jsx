import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from './store/store.js';
import { receiveTodo, receiveTodos, fetchTodos } from './actions/todo_actions';
import Root from './components/root';
import { allTodos } from './reducers/selectors';
import { ajaxRequest } from './util/todo_api_util';

const store = configureStore();

document.addEventListener('DOMContentLoaded', () => {
  window.store = store;
  window.receiveTodo = receiveTodo;
  window.receiveTodos = receiveTodos;
  window.allTodos = allTodos;
  window.ajaxRequest = ajaxRequest;
  window.fetchTodos = fetchTodos;
  const main = document.querySelector('#root');
  ReactDOM.render(<Root store={store} />, main);
});
