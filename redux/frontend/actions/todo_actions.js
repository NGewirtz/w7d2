import * as APIUtil from '../util/todo_api_util';
import { receiveErrors, clearErrors } from './error_actions';

export const RECEIVE_TODOS = 'RECEIVE_TODOS';
export const RECEIVE_TODO = 'RECEIVE_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';
export const UPDATE_TODO = 'UPDATE_TODO';


export const receiveTodos = (todos) => {
  return {
    type: RECEIVE_TODOS,
    todos: todos
  };
};

export const receiveTodo = (todo) => {
  return {
    type: RECEIVE_TODO,
    todo: todo
  };
};

export const removeTodo = ({ id }) => {
  return {
    type: REMOVE_TODO,
    id: id
  };
};

export const updateTodo = (todo) => {
  return {
    type: UPDATE_TODO,
    todo: todo
  };
};

export const fetchTodos = () => {
  return (dispatch) => {
    return APIUtil.ajaxRequest().then(todos => {
      return dispatch(receiveTodos(todos));
    });
  };
};

export const createTodo = (todo) => {
  return (dispatch) => {
    return APIUtil.ajaxPost(todo).then(
      (todo) => {
        dispatch(clearErrors());
        return dispatch(receiveTodo(todo));
      }, (err) => {
        return dispatch(receiveErrors(err.responseJSON));
      }
    );
  };
};


export const toggleTodo = (todo) => {
  return (dispatch) => {
    return APIUtil.ajaxUpdate(todo).then( (todo) => {
        return dispatch(receiveTodo(todo));
    });
  };
};

export const deleteTodo = (todo) => {
  return (dispatch) => {
    return APIUtil.ajaxDelete(todo).then( (todoResp) => {
        return dispatch(removeTodo(todo));
    });
  };
};
