export const ajaxRequest = () => {
  return $.ajax('/api/todos');
};

export const ajaxPost = (todo) => {
  return $.ajax({
    method: 'POST',
    url: '/api/todos',
    dataType: 'JSON',
    data: { todo: todo}
  });
};

export const ajaxUpdate = (todo) => {
  todo.done = !todo.done;
  return $.ajax({
    method: 'PATCH',
    url: `/api/todos/${todo.id}`,
    dataType: 'JSON',
    data: { todo: todo }
  });
};

export const ajaxDelete = (todo) => {
  return $.ajax({
    method: 'DELETE',
    url: `/api/todos/${todo.id}`,
    dataType: 'JSON',
    data: { todo: todo }
  });
};
