export const thunk = (store) => {
  const dispatch = store.dispatch;
  const getState = store.getState;
  return (next) => {
    return (action) => {
      if (typeof action === 'function') {
        return action(dispatch, getState);
      } else {
        return next(action);
      }
    };
  };
};
