import React from 'react';

class StepListForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      title: ''
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.receiveStep({
      id: 3,
      title: this.state.title,
      done: false,
      todoId: this.props.todoId
    });
  }

  linkState(key) {
    return (event => this.setState({[key]: event.currentTarget.value}));
  }

  render() {
    return (
      <form onSubmit = {this.handleSubmit}>
        <input type = 'text' placeholder="title" onChange={this.linkState('title')} value={this.state.title} />
        <input type = 'submit' value = "Add Step"></input>

      </form>
    );
  }
}

export default StepListForm;
