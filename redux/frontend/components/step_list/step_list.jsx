import React from 'react';

const StepList = (props) => {
  const stepItems = props.steps.map((step) => {
    return <li key={step.id}>{step.body}</li>;
  });
  return (
    <ul>
      {stepItems}
    </ul>
  );
};



export default StepList;
