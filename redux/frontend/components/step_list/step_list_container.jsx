import StepList from './step_list';
import { connect } from 'react-redux';
import { stepsByTodoId } from '../../reducers/selectors';
import { receiveStep } from '../../actions/step_actions';

const mapDispatchToProps = (dispatch) => {
  return {
    receiveStep: (step) => dispatch(receiveStep(step))
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    steps: stepsByTodoId(state, ownProps.todoId),
    todoId: ownProps.todoId
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StepList);
