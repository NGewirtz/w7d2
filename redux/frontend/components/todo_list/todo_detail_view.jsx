import React from 'react';
import StepListContainer from '../step_list/step_list_container';
import StepListForm from '../step_list/step_form';

class TodoDetailView extends React.Component {

  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleDelete(e) {
    e.preventDefault();
    return this.props.deleteTodo(this.props.todo);
  }

  render() {
    if(this.props.detail)
      return (
        <div>
          <p>{ this.props.todo.body}</p>
          <button onClick = {this.handleDelete}>Remove This Item</button>
          <StepListContainer todoId = {this.props.todo.id} />
          <StepListForm todoId = {this.props.todo.id} receiveStep = {this.props.receiveStep}/>
        </div>
      );
    else {
      return (
        <p></p>
      );
    }
  }


}

export default TodoDetailView;
