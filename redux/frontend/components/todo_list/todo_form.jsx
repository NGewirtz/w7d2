import React from 'react';
import { uniqueId } from '../../util/util';

class TodoForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      body: ''
    };
    this.linkState = this.linkState.bind(this);
    this.createTodoObj = this.createTodoObj.bind(this);
    this.handleTodo = this.handleTodo.bind(this);
  }

  linkState(key) {
    return (event => this.setState({[key]: event.currentTarget.value}));
  }

  createTodoObj() {
    return {
      title: this.state.title,
      id: uniqueId(),
      body: this.state.body,
      done: false
    };
  }

  handleTodo(e) {
    e.preventDefault();
    const newTodo = this.props.createTodo(this.createTodoObj()).then(() => {
      this.setState({title: '', body: ''});
      // this.props.clearErrors();
    },
    (err) => {
      console.log("hello!!");
    }
  );
    return newTodo;
  }

  render() {
    return (
      <form onSubmit={this.handleTodo}>
        <p>{this.props.errors.length > 0 && this.props.errors}</p>
        <input type = 'text' placeholder="title" onChange={this.linkState('title')} value={this.state.title}/>
        <input type = 'text' placeholder="description" onChange={this.linkState('body')} value={this.state.body}/>
        <button>Submit</button>
      </form>
    );
  }
}

export default TodoForm;
