import { connect } from 'react-redux';
import TodoList from './todo_list';
import { allTodos } from '../../reducers/selectors';
import { removeTodo, updateTodo, fetchTodos, createTodo, toggleTodo } from '../../actions/todo_actions';
import { clearErrors } from '../../actions/error_actions';

const mapStateToProps = (state) => {
  return {
    todos: allTodos(state),
    errors: state.errors
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createTodo: (todo) => dispatch(createTodo(todo)),
    removeTodo: (id) => dispatch(removeTodo(id)),
    updateTodo: (todo) => dispatch(updateTodo(todo)),
    fetchTodos: () => dispatch(fetchTodos()),
    clearErrors: () => dispatch(clearErrors()),
    toggleTodo: (todo) => dispatch(toggleTodo(todo))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
