import React from 'react';
import TodoListItem from './todo_list_item';
import TodoForm from './todo_form';

class TodoList extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchTodos();
  }

  render() {
    const listItems = this.props.todos.map((el, idx) => (<TodoListItem
       key={el.id}
       todo={el}
       toggleTodo={this.props.toggleTodo}
       removeTodo={this.props.removeTodo}/>));
    return (
      <div>
        <ul>
          {listItems}
        </ul>
        <TodoForm createTodo={this.props.createTodo} errors={this.props.errors} clearErrors={this.props.clearErrors}/>
      </div>
    );
  }
}


export default TodoList;
