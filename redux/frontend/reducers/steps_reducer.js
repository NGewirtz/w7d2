import { RECEIVE_STEPS, RECEIVE_STEP, REMOVE_STEP } from '../actions/step_actions';

const stepReducer = (state = {}, action) => {
  switch (action.type) {
    case RECEIVE_STEPS: {
      const newObj = {};
      action.steps.forEach((step) => {
        newObj[step.id] = step;
      });
      return newObj;
    }
    case RECEIVE_STEP: {
      const newObj = Object.assign({}, state, { [action.step.id]: action.step });
      return newObj;
    }
    case REMOVE_STEP: {
      const newState = Object.assign({}, state);
      delete newState[action.id];
      return newState;
    }
    default:
      return state;
  }
};

export default stepReducer;
