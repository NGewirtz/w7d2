export const allTodos = (state) => {
  return Object.keys(state.todos).map((el) => {
    return state.todos[el];
  });
};

export const stepsByTodoId = (state, todoId) => {
  const arr = [];
  const keys = Object.keys(state.steps).filter( (stepId) => {
    return state.steps[stepId][todoId] === todoId;
  });
  keys.forEach((key) => {
    arr.push(state.steps[key]);
  });
  return arr;
};
